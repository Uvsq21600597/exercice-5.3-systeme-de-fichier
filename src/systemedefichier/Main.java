package systemedefichier;



public class Main {
	
	public static void main(String[] args) {
		
		Fichier f1 = new Fichier(10,"F1");
		Fichier f2 = new Fichier(15,"F2");
		Fichier f3 = new Fichier(20,"F3");
		Fichier f4 = new Fichier(25,"F4");
		
		Repertoire r1 =new Repertoire(0, "R1");
		Repertoire r2 =new Repertoire(0, "R2");
		Repertoire r3 =new Repertoire(0, "R3");
		
		r1.Add(f1);
		r1.Add(f2);
		r1.Add(f3);
		r2.Add(f4);
		r2.Add(f1);
		
	
		
		System.out.println("la taille de R1 est : "+r1.CalculeTaille());
		System.out.println("la taille de R2 est : "+r2.CalculeTaille());
		
		
		Repertoire r4 =new Repertoire(0, "R4");
		Repertoire r5 =new Repertoire(0, "R5");
		
		r1.Add(r2);
		r3.Add(r5);
		r3.Add(r4);
		
		r2.Add(r2);
		r5.Add(r3);
	
	}

}
